import { Component } from 'react';
import { Link } from 'react-router-dom';
import { Layout, Menu } from 'antd';
import {
  FileTextOutlined,
  AuditOutlined,
  FileDoneOutlined,
  FileOutlined,
} from '@ant-design/icons';

const { Sider } = Layout;
const { SubMenu } = Menu;

class PageSider extends Component {
  constructor(props) {
    super(props);
    this.state = { collapsed: false };
    this.onCollapse = this.onCollapse.bind(this);
    this.toggle = this.toggle.bind(this);
  }

  onCollapse(collapsed) {
    this.setState({ collapsed });
  }

  toggle() {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  }

  render() {
    return (
      <Sider
        collapsible
        collapsed={this.state.collapsed}
        onCollapse={this.onCollapse}
        width={400}>
        <div className="logo" />
        <Menu
          theme="dark"
          defaultSelectedKeys={['menu1']}
          mode="inline"
          defaultOpenKeys={['menu1']}
          >
          <SubMenu key="menu1" icon={<FileTextOutlined />} title="PHẦN I: LÝ THUYẾT">
            <Menu.Item key="menu1-1" icon={<FileOutlined />}>
              <span>I. Mục đích, yêu cầu</span>
              <Link to="/muc-dich-yeu-cau" />
              </Menu.Item>
            <SubMenu key="menu1-2" icon={<FileOutlined />} title="II. Đại cương phản vệ">
              <Menu.Item key="menu1-2-1">
                <span>1. Khái niệm</span>
                <Link to="/dai-cuong-phan-ve-khai-niem" />
              </Menu.Item>
              <Menu.Item key="menu1-2-2">
                <span>2. Nguyên tắc dự phòng phản vệ</span>
                <Link to="/nguyen-tac-du-phong-phan-ve" />
              </Menu.Item>
              <Menu.Item key="menu1-2-3">
                <span>3. Chuẩn bị, dự phòng cấp cứu phản vệ</span>
                <Link to="/chuan-bi-du-phong-cap-cuu-phan-ve" />
              </Menu.Item>
              <Menu.Item key="menu1-2-4">
                <span>4. Nguyên tắc xử trí</span>
                <Link to="/nguyen-tac-xu-tri-phan-ve" />
              </Menu.Item>
            </SubMenu>
            <SubMenu key="menu1-3" icon={<FileOutlined />} title="III. Chuẩn đoán phản vệ">
              <Menu.Item key="menu1-3-1" >
                <span>1. Triệu chứng gợi ý</span>
                <Link to="/trieu-chung-goi-y-phan-ve" />
              </Menu.Item>
              <Menu.Item key="menu1-3-2">
                <span>2. Các bệnh cảnh lâm sàng</span>
                <Link to="/cac-benh-canh-lam-sang" /></Menu.Item>
            </SubMenu>
            <Menu.Item key="menu1-4" icon={<FileOutlined />}>IV. Chẩn đoán phân biệt</Menu.Item>
            <Menu.Item key="menu1-5" icon={<FileOutlined />}>V. Hướng dẫn chẩn đoán mức độ phản vệ</Menu.Item>
            <SubMenu key="menu1-6" icon={<FileOutlined />} title="VI. Hướng dẫn xử trí cấp cứu phản vệ">
              <Menu.Item key="menu1-6-1">1. Nguyên tắc chung</Menu.Item>
              <Menu.Item key="menu1-6-2">2. Xử trí phản vệ nhẹ độ 1</Menu.Item>
              <Menu.Item key="menu1-6-3">3. Phác đồ xử trí cấp cứu phản vệ mức nặng và nguy kịch (độ II, III)</Menu.Item>
              <Menu.Item key="menu1-6-4">4. Phác đồ sử dụng adrenalin và truyền dịch</Menu.Item>
              <Menu.Item key="menu1-6-5">4. Xử trí tiếp theo và theo dõi</Menu.Item>
            </SubMenu>
            <Menu.Item key="menu1-7" icon={<FileOutlined />}>VII. Hướng dẫn xử trí phản vệ trong một số trường hợp đặc biệt</Menu.Item>
            <Menu.Item key="menu1-8" icon={<FileOutlined />}>VIII. Hộp thuốc cấp cứu phản vệ và trang thiết bị y tế</Menu.Item>
            <SubMenu key="menu1-9" icon={<FileOutlined />} title="IX. Mẫu phiếu theo dõi khai thác tiền sử dị ứng">
              <Menu.Item key="menu1-9-1">1. Mẫu phiếu khai thác tiền sử dị ứng</Menu.Item>
              <Menu.Item key="menu1-9-2">2. Mẫu thẻ theo dõi dị ứng</Menu.Item>
            </SubMenu>
            <Menu.Item key="menu1-10" icon={<FileOutlined />}>X. Một số lưu ý</Menu.Item>
          </SubMenu>

          <SubMenu key="menu2" icon={<AuditOutlined />} title="PHẦN II: THỰC HÀNH">
            <SubMenu key="menu2-1" icon={<FileOutlined />} title="I. Hướng dẫn chỉ định làm test da">
            </SubMenu>
            <SubMenu key="menu2-2" icon={<FileOutlined />} title="II. Quy trình kỹ thuật test da">
              <Menu.Item key="menu2-2-1">1. Test lẩy da</Menu.Item>
              <Menu.Item key="menu2-2-2">2. Test nội bì</Menu.Item>
            </SubMenu>
            <SubMenu key="menu2-3" icon={<FileOutlined />} title="III. Một số hình ảnh minh họa qui trình kỹ thuật test da">
            </SubMenu>
            <SubMenu key="menu2-4" icon={<FileOutlined />} title="IV. Video thực hành kỹ thuật test da">
            </SubMenu>
          </SubMenu>

          <SubMenu key="menu3" icon={<FileDoneOutlined />} title="PHẦN III: KIỂM TRA ">
            <SubMenu key="menu3-1" icon={<FileOutlined />} title="I. Thực hành vẽ sơ đồ chi tiết về chẩn đoán và xử trí phản vệ">
            </SubMenu>
            <SubMenu key="menu3-2" icon={<FileOutlined />} title="II. Thực hành vẽ sơ đồ tóm tắt về chẩn đoán và xử trí phản vệ">
            </SubMenu>
            <SubMenu key="menu3-3" icon={<FileOutlined />} title="III. Câu hỏi trắc nghiệm">
            </SubMenu>
          </SubMenu>
        </Menu>
      </Sider>
    );
  }
}

export default PageSider;
