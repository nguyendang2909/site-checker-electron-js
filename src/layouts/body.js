import { Component } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import { Layout } from 'antd';
import Homepage from '../containers/homepage';
import Meseros from '../containers/lectures/muc-dich-yeu-cau';
import DaiCuongPhanVeKhaiNiem from '../containers/lectures/khai-niem-phan-ve';
import NguyenTacDuPhongPhanVe from '../containers/lectures/nguyen-tac-du-phong-phan-ve';
import ChuanBiDuPhongCapCuuPhanVe from '../containers/lectures/chuan-bi-du-phong-cap-cuu-phan-ve';
import NguyenTacXuTriPhanVe from '../containers/lectures/nguyen-tac-xu-tri-phan-ve';
import TrieuChungGoiYPhanVe from '../containers/lectures/trieu-chung-goi-y-phan-ve';
import CacBenhCanhLamSang from '../containers/lectures/cac-benh-canh-lam-sang';
import DanhMucChuVietTat from '../containers/danh-muc-chu-viet-tat';
import GioiThieu from '../containers/gioi-thieu';

const { Content } = Layout;

class PageBody extends Component {
  render() {
    return (
    <Content style={{
      margin: '24px 16px', padding: 24, background: '#fff', minHeight: 280,
    }}>
        <Switch>
          <Route exact path="/" render={() => (<Redirect to="/home" />)} />
          <Route exact path="/home" component={Homepage} />
          <Route exact path="/gioi-thieu" component={GioiThieu} />
          <Route exact path="/danh-muc-chu-viet-tat" component={DanhMucChuVietTat} />

          <Route exact path="/muc-dich-yeu-cau" component={Meseros} />
          <Route exact path="/dai-cuong-phan-ve-khai-niem" component={DaiCuongPhanVeKhaiNiem} />
          <Route exact path="/nguyen-tac-du-phong-phan-ve" component={NguyenTacDuPhongPhanVe} />
          <Route exact path="/chuan-bi-du-phong-cap-cuu-phan-ve" component={ChuanBiDuPhongCapCuuPhanVe} />
          <Route exact path="/nguyen-tac-xu-tri-phan-ve" component={NguyenTacXuTriPhanVe} />
          <Route exact path="/trieu-chung-goi-y-phan-ve" component={TrieuChungGoiYPhanVe} />
          <Route exact path="/cac-benh-canh-lam-sang" component={CacBenhCanhLamSang} />
        </Switch>
    </Content>
    );
  }
}

export default PageBody;
