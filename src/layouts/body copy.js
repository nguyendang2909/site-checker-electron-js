import React from 'react';
import { Layout, Menu } from 'antd';
import { UserOutlined, LaptopOutlined, NotificationOutlined, BookOutlined, ProfileTwoTone } from '@ant-design/icons';

const { Content, Sider } = Layout;
const { SubMenu } = Menu;

class Body extends React.Component {
  constructor(props) {
    super(props);
    this.handleMenuClick = this.handleMenuClick.bind(this);
    this.state = {
      content: ''
    }
  }

  handleMenuClick(e) {
    console.log(e.key);
    this.setState({
      content: 'aaaa'
    })
  }

  render() {
    return (
      <Content style={{ padding: '0 50px' }}>
        <Layout className="site-layout-background" style={{ padding: '24px 0' }}>
          <Sider className="site-layout-background" width={400}>
            <Menu
              mode="inline"
              defaultSelectedKeys={['1']}
              defaultOpenKeys={['menu1']}
              style={{ height: '100%' }}
              onClick={(e) => {
                this.handleMenuClick(e);
              }}
            >
              <SubMenu key="menu1" icon={<ProfileTwoTone />} title="PHẦN I: LÝ THUYẾT">
                <Menu.Item key="menu1-1" icon={<BookOutlined />}>I. Mục đích, yêu cầu</Menu.Item>
                <SubMenu key="menu12" icon={<BookOutlined />} title="II. Đại cương phản vệ">
                  <Menu.Item key="menu1-2-1">1. Khái niệm </Menu.Item>
                  <Menu.Item key="menu1-2-2">2. Nguyên tắc dự phòng phản vệ </Menu.Item>
                  <Menu.Item key="menu1-2-3">3. Chuẩn bị, dự phòng cấp cứu phản vệ</Menu.Item>
                  <Menu.Item key="menu1-2-4">4. Nguyên tắc xử trí</Menu.Item>
                </SubMenu>
                <SubMenu key="menu1-3" icon={<BookOutlined />} title="III. Chuẩn đoán phản vệ">
                  <Menu.Item key="menu1-3-1" >1. Triệu chứng gợi ý</Menu.Item>
                  <Menu.Item key="menu1-3-2">2. Các bệnh cảnh lâm sàng</Menu.Item>
                </SubMenu>
                <Menu.Item key="menu1-4" icon={<BookOutlined />}>IV. Chẩn đoán phân biệt</Menu.Item>
                <Menu.Item key="menu1-5" icon={<BookOutlined />}>V. Hướng dẫn chẩn đoán mức độ phản vệ</Menu.Item>
                <SubMenu key="menu1-6" icon={<BookOutlined />} title="VI. Hướng dẫn xử trí cấp cứu phản vệ">
                  <Menu.Item key="menu1-6-1">1. Nguyên tắc chung</Menu.Item>
                  <Menu.Item key="menu1-6-2">2. Xử trí phản vệ nhẹ độ 1</Menu.Item>
                  <Menu.Item key="menu1-6-3">3. Phác đồ xử trí cấp cứu phản vệ mức nặng và nguy kịch (độ II, III)</Menu.Item>
                  <Menu.Item key="menu1-6-4">4. Phác đồ sử dụng adrenalin và truyền dịch</Menu.Item>
                  <Menu.Item key="menu1-6-5">4. Xử trí tiếp theo và theo dõi</Menu.Item>
                </SubMenu>
                <Menu.Item key="menu1-7" icon={<BookOutlined />}>VII. Hướng dẫn xử trí phản vệ trong một số trường hợp đặc biệt</Menu.Item>
                <Menu.Item key="menu1-8" icon={<BookOutlined />}>VIII. Hộp thuốc cấp cứu phản vệ và trang thiết bị y tế</Menu.Item>
                <SubMenu key="menu1-9" icon={<BookOutlined />} title="IX. Mẫu phiếu theo dõi khai thác tiền sử dị ứng">
                  <Menu.Item key="menu1-9-1">1. Mẫu phiếu khai thác tiền sử dị ứng</Menu.Item>
                  <Menu.Item key="menu1-9-2">2. Mẫu thẻ theo dõi dị ứng</Menu.Item>
                </SubMenu>
                <Menu.Item key="menu1-10" icon={<BookOutlined />}>X. Một số lưu ý</Menu.Item>
              </SubMenu>
              <SubMenu key="menu2" icon={<UserOutlined />} title="PHẦN II: THỰC HÀNH">
                <SubMenu key="menu2-1" icon={<NotificationOutlined />} title="I. Hướng dẫn chỉ định làm test da">
                </SubMenu>
                <SubMenu key="menu2-2" icon={<LaptopOutlined />} title="II. Quy trình kỹ thuật test da">
                  <Menu.Item key="menu2-2-1">1. Test lẩy da</Menu.Item>
                  <Menu.Item key="menu2-2-2">2. Test nội bì</Menu.Item>
                </SubMenu>
                <SubMenu key="menu2-3" icon={<NotificationOutlined />} title="III. Một số hình ảnh minh họa qui trình kỹ thuật test da">
                </SubMenu>
                <SubMenu key="menu2-4" icon={<NotificationOutlined />} title="IV. Video thực hành kỹ thuật test da">
                </SubMenu>
              </SubMenu>
              <SubMenu key="menu3" icon={<UserOutlined />} title="PHẦN III: KIỂM TRA ">
                <SubMenu key="menu3-1" icon={<NotificationOutlined />} title="I. Thực hành vẽ sơ đồ chi tiết về chẩn đoán và xử trí phản vệ">
                </SubMenu>
                <SubMenu key="menu3-2" icon={<NotificationOutlined />} title="II. Thực hành vẽ sơ đồ tóm tắt về chẩn đoán và xử trí phản vệ">
                </SubMenu>
                <SubMenu key="menu3-3" icon={<NotificationOutlined />} title="III. Câu hỏi trắc nghiệm">
                </SubMenu>
              </SubMenu>
            </Menu>
          </Sider>
          <Content style={{ padding: '0 24px', minHeight: 700 }}>{this.state.content}</Content>
        </Layout>
      </Content>
    )
  }

}

export default Body;