import { Component } from 'react';
import { Layout } from 'antd';

const { Footer } = Layout;

class SiteFooter extends Component {
  render() {
    return (
      <Footer style={{ textAlign: 'center' }}>Copyright ©2021 Created by Lê Ngọc Lan</Footer>
    );
  }
}

export default SiteFooter;
