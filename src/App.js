/* eslint-disable import/no-named-as-default */
/* eslint-disable import/no-named-as-default-member */
import React, { Component } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { Layout } from 'antd';
import './index.css';
import 'antd/dist/antd.css';
import PageHeader from './layouts/header';
import PageBody from './layouts/body';
import PageFooter from './layouts/footer';
import PageSider from './layouts/sider';

class RouterApp extends Component {
  render() {
    return (
        <Router>
          <Layout style={{ minHeight: '100vh' }}>
            <PageSider />
            <Layout>
              <PageHeader />
              <PageBody />
              <PageFooter />
            </Layout>
          </Layout>
        </Router>
    );
  }
}

export default RouterApp;
