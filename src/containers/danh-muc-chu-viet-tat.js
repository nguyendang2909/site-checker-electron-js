import { Component } from 'react';
import { Table } from 'antd';

const columns = [
  {
    title: 'Từ khóa',
    dataIndex: 'keyword',
    key: 'keyword',
  },
  {
    title: 'Ý nghĩa',
    dataIndex: 'name',
    key: 'name',
  },
];

const data = [
  {
    keyword: 'PKKQ',
    name: 'Phòng không – Không quân',
  },
  {
    keyword: 'NB',
    name: 'Người bệnh',
  },
  {
    keyword: 'NNNB',
    name: 'Người nhà người bệnh',
  },
  {
    keyword: 'ĐTNC',
    name: 'Đối tượng nghiên cứu',
  },
  {
    keyword: 'ĐH',
    name: 'Đại học',
  },
  {
    keyword: 'CĐ',
    name: 'Cao đẳng',
  },
  {
    keyword: 'PKKasdasdasdasdasdasdaQ',
    name: 'Pasadasdasdasda',
  },
  {
    keyword: 'TT',
    name: 'Thông tư',
  },
  {
    keyword: 'PV',
    name: 'Phản vệ',
  },
  {
    keyword: 'PKKasdasdasdasdasdasdaQ',
    name: 'Pasadasdasdasda',
  },
  {
    keyword: 'SPV',
    name: 'Sốc phản vệ',
  },
  {
    keyword: 'KT',
    name: 'Kiến thức',
  },
  {
    keyword: 'SL',
    name: 'Số lượng',
  },
  {
    keyword: 'TL',
    name: 'Tỷ lệ',
  },
];

class Content extends Component {
  render() {
    return (
      <div>
        <Table columns={columns} dataSource={data} />
      </div>
    );
  }
}

export default Content;
