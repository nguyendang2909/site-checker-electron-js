export default function App() {
  return (
    <div>
      <h3 align="center">CỤC HẬU CẦN PHÒNG KHÔNG – KHÔNG QUÂN
      <br /><strong>VIỆN Y HỌC PHÒNG KHÔNG – KHÔNG QUÂN</strong></h3>

      <br /><br /> <br /><h3 align="center"><strong>LÊ THỊ NGỌC LAN</strong></h3>
      <br /><br /><h2 align="center"><strong>THỰC TRẠNG KIẾN THỨC DỰ PHÒNG, CHUẨN ĐOÁN VÀ XỬ TRÍ PHẢN VỆ THEO THÔNG TƯ SỐ 51/2017/TT-BYT CỦA ĐIỀU DƯỠNG VIỆN Y HỌC PHÒNG KHÔNG - KHÔNG QUÂN NĂM 2020</strong></h2>
      <br /><br /><h2 align="center"><strong>Chuyên ngành: Điều dưỡng</strong></h2>
      <br/><h2 align="center"><strong>ĐỀ TÀI NGHIÊN CỨU KHOA HỌC CẤP CƠ SỞ</strong></h2>
      <br /><br /><br /><h3 align="center"><strong>Hà Nội – Tháng 8 năm 2020</strong></h3>
    </div>
  );
}
