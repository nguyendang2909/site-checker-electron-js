import { Component } from 'react';

class Content extends Component {
  render() {
    return (
      <div>
        <p align="justify">
          1. Adrenalin là thuốc thiết yếu, quan trọng hàng đầu, sẵn có để sử dụng cấp cứu phản vệ.
          <br /><br />2. Nơi có sử dụng thuốc, xe tiêm phải được trang bị và sẵn sàng hộp thuốc cấp cứu phản vệ. Thành phần hộp thuốc cấp cứu phản vệ theo quy định.
          <br /><br />3. Cơ sở khám bệnh, chữa bệnh phải có hộp thuốc cấp cứu phản vệ và trang thiết bị y tế theo quy định.
          <br /><br />4. Bác sĩ, nhân viên y tế phải nắm vững kiến thức và thực hành được cấp cứu phản vệ theo phác đồ.
          <br /><br />5. Trên các phương tiện giao thông công cộng máy bay, tàu thủy, tàu hỏa, cần trang bị hộp thuốc cấp cứu phản vệ.

        </p>
      </div>
    );
  }
}

export default Content;
