import { Component } from 'react';

class Content extends Component {
  render() {
    return (
      <div>
        <p align="justify">- Tất cả các trường hợp phản vệ phải được phát hiện sớm, xử trí khẩn cấp, kịp thời, ngay tại chỗ và theo dõi liên tục, ít nhất trong vòng 24h.</p>
        <p align="justify">- Bác sĩ, điều dưỡng, hộ sinh viên, kỹ thuật viên, nhân viên y tế khác phải xử trí ban đầu cấp cứu phản vệ.</p>
        <p align="justify">- Adrenalin là thuốc thiết yếu, quan trọng hàng đầu cứu sống người bệnh bị phản vệ, phải được tiêm bắp ngay khi chẩn đoán phản vệ từ độ II trở lên.</p>
      </div>
    );
  }
}

export default Content;
