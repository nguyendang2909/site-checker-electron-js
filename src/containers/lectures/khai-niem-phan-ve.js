import { Component } from 'react';

class Content extends Component {
  render() {
    return (
      <div>
        <h3><strong>1.1.1. Khái niệm phản vệ, dị nguyên, sốc phản vệ [4]</strong></h3>
        <p align="justify">- Phản vệ là một phản ứng dị ứng, có thể xuất hiện ngay lập tức từ vài giây, vài phút đến vài giờ sau khi cơ thể tiếp xúc với dị nguyên gây ra các bệnh cảnh lâm sàng khác nhau, có thể nghiêm trọng dẫn đến tử vong nhanh chóng.</p>
        <p align="justify">- Dị nguyên là yếu tố lạ khi tiếp xúc có khả năng gây phản ứng dị ứng cho cơ thể, bao gồm thức ăn, thuốc và các yếu tố khác.</p>
        <p align="justify">- Sốc phản vệ là mức độ nặng nhất của phản vệ do đột ngột giãn toàn bộ hệ thống mạch và co thắt phế quản có thể gây tử vong trong vòng một vài phút.</p>

        <h3><strong>1.1.2. Nguyên nhân gây phản vệ [8]</strong></h3>
        <p align="justify">- Nhóm thứ nhất là vacxin. huyết thanh. kháng sinh và nhiều thuốc khác (vitamin B1, Novocain, sulefamid, vv).</p>
        <p align="justify">- Nhóm thứ hai là nọc côn trùng (ong mật, ong vàng, ong vò vẽ, vv).</p>
        <p align="justify">- Nhóm thứ ba là nhiều loại thực phẩm nguồn động vật và thực vật (sữa bò, trứng gà, cá, dầu hướng dương, rượu, vv).</p>
        <p align="justify">Dị nguyên là thuốc: phản vệ là những tai biến do dị ứng thuốc xảy ra ngày một nhiều, với những hậu quả rất nghiêm trọng. Khá nhiều loại thuốc có thể gây phản vệ như: kháng sinh, vacxin và huyết thanh, các thuốc giảm đau, hạ nhiệt, chống viêm không steroit, một số loại vitamin vv, Những năm gần đây có các ca phản vệ do dùng các thuốc gây mê và gây tê.</p>
        <p align="justify">Phản vệ do nọc côn trùng: do ong đốt, kiến đốt.</p>
        <p align="justify">Phản vệ do thức ăn:  thức ăn nguồn động vật, thực vật có thể gây nên các hội chứng dị ứng như mày đay, mẩn ngứa, viêm mũi, viêm miệng, vv…nhưng ít gây sốc phản vệ. Sữa bò, trứng gà, tôm, cua, cá, ốc có thể gây nên phản vệ vì chúng là dị nguyên có tính kháng nguyên khá mạnh. Sữa bò có nhiều thành phần protein khác nhau: betalactoglobulin (A và B), anphalactoanbumin, cazein (anpha, gama), trong đó betalactoglobulin (A và B) có tính khnág nguyên mạnh hơn cả. Sữa bò gây nên nhiều hội chứng dị ứng như phản vệ, hen phế quản, viêm mũi dị ứng, rối loạn tiêu hoá theo cơ chế dị ứng, mày đay, phù Quincke, sốt, vv…</p>
        <p align="justify">Phản vệ do yếu tố lạnh: một số bệnh nhân bị dị ứng do lạnh, khi tắm hoặc làm việc lâu ở sông hoặc ở biển, hồ vào thời tiết lạnh, có thể xuất hiện phản vệ</p>

        <h3><strong>1.1.3. Triệu chứng lâm sàng của phản vệ [4]</strong></h3>
        <h4><strong>1.1.3.1. Triệu chứng gợi ý</strong></h4>
        <p align="justify">Nghĩ đến phản vệ khi xuất hiện ít nhất một trong các triệu chứng sau:</p>
        <p align="justify">- Mày đay, phù mạch nhanh</p>
        <p align="justify">- Khó thở, tức ngực, thở rít</p>
        <p align="justify">- Đau bụng hoặc nôn</p>
        <p align="justify">- Tụt huyết áp hoặc ngất</p>
        <p align="justify">- Rối loạn ý thức</p>

        <h4><strong>1.1.3.2. Các bệnh cảnh lâm sàng</strong></h4>
        <p align="justify">* <i>Bệnh cảnh lâm sàng 1</i>: Các triệu chứng xuất hiện trong vài giây đến vài giờ ở da, niêm mạc (mày đay, phù mạch, ngứa,,,) và có ít nhất 1 trong 2 triệu chứng sau:</p>
        <p align="justify">-  Các triệu chứng hô hấp (khó thở, thở rít, ran rít)</p>
        <p align="justify">- Tụt huyết áp (HA) hay các hậu quả của tụt HA (rối loạn ý thức, đại tiện, tiểu tiện không tự chủ,,,)</p>
        <p align="justify">* <i>Bệnh cảnh lâm sàng 2</i>: Ít nhất 2 trong 4 triệu chứng sau xuất hiện trong vài giây đến vài giờ sau khi người bệnh tiếp xúc với yếu tố nghi ngờ:</p>
        <p align="justify">- Biểu hiện ở da, niêm mạc: mày đay, phù mạch, ngứa</p>
        <p align="justify">- Các triệu chứng hô hấp (khó thở, thở rít, ran rít)</p>
        <p align="justify">- Tụt huyết áp hoặc các hậu quả của tụt huyết áp (rối loạn ý thức, đại tiện, tiểu tiện không tự chủ,,,)</p>
        <p align="justify">- Các triệu chứng tiêu hóa (nôn, đau bụng,,,)</p>
        <p align="justify">* <i>Bệnh cảnh lâm sàng 3</i>: Tụt huyết áp xuất hiện trong vài giây đến vài giờ sau khi tiếp xúc với yếu tố nghi ngờ mà người bệnh đã từng bị dị ứng:</p>
        <p align="justify">- Trẻ em: giảm ít nhất 30% huyết áp tâm thu (HA tối đa) hoặc tụt huyết áp tâm thu so với tuổi (huyết áp tâm thu nhỏ hơn 70mmHg).</p>
        <p align="justify">- Người lớn: Huyết áp tâm thu &lt; 90mmHg hoặc giảm 30% giá trị huyết áp tâm thu nền.</p>

        <h3><strong>1.1.4. Chẩn đoán phân biệt [4]</strong></h3>
        <p align="justify">- Các trường hợp sốc: sốc tim, sốc giảm thể tích, sốc nhiễm khuẩn.</p>
        <p align="justify">- Tai biến mạch máu não.</p>
        <p align="justify">- Các nguyên nhân đường hô hấp: COPD, cơn hen phế quản, khó thở thanh quản (do dị vật, viêm).</p>
        <p align="justify">- Các bệnh lý ở da: mày đay, phù mạch.</p>
        <p align="justify">- Các bệnh lý nội tiết: cơn bão giáp trạng, hội chứng carcinoid, hạ đường máu.</p>
        <p align="justify">- Các ngộ độc: rượu, opiat, histamine./.</p>

        <h3><strong>1.1.5. Chẩn đoán mức độ phản vệ [4]</strong></h3>
        <p align="justify">Phản vệ được phân thành 4 mức độ như sau: (lưu ý mức độ phản vệ có thể nặng lên rất nhanh và không theo tuần tự)</p>
        <p align="justify">- Nhẹ (độ I): Chỉ có các triệu chứng da, tổ chức dưới da và niêm mạc như mày đay, ngứa, phù mạch.</p>
        <p align="justify">- Nặng (độ II): có từ 2 biểu hiện ở nhiều cơ quan:</p>
        <p align="justify">+  Mày đay, phù mạch xuất hiện nhanh.</p>
        <p align="justify">+  Khó thở nhanh nông, tức ngực, khàn tiếng, chảy nước mũi.</p>
        <p align="justify">+ Đau bụng, nôn, ỉa chảy.</p>
        <p align="justify">+ Huyết áp chưa tụt hoặc tăng, nhịp tim nhanh hoặc loạn nhịp.</p>
        <p align="justify">- Nguy kịch (độ III): biểu hiện ở nhiều cơ quan với mức độ nặng hơn như sau:</p>
        <p align="justify">+ Đường thở: tiếng rít thanh quản, phù thanh quản.</p>
        <p align="justify">+ Thở: thở nhanh, khò khè, tím tái, rối loạn nhịp thở.</p>
        <p align="justify">+ Rối loạn ý thức: vật vã, hôn mê, co giật, rối loạn cơ tròn.</p>
        <p align="justify">- Tuần hoàn: sốc, mạch nhanh nhỏ, tụt huyết áp.</p>
        <p align="justify">- Ngừng tuần hoàn (độ IV): Biểu hiện ngừng hô hấp, ngừng tuần hoàn./.</p>
      </div>
    );
  }
}

export default Content;
