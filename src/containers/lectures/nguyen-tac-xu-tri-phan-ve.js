import { Component } from 'react';
import { Table } from 'antd';

const columns = [
  {
    title: 'STT',
    dataIndex: 'stt',
    key: 'stt',
  },
  {
    title: 'Nội dung',
    dataIndex: 'noiDung',
    key: 'noiDung',
  },
  {
    title: 'Đơn vị',
    dataIndex: 'donVi',
    key: 'donVi',
  },
  {
    title: 'Số lượng',
    dataIndex: 'soLuong',
    key: 'soLuong',
  },
];

const data = [
  {
    stt: '1',
    noiDung: 'Phác đồ, sơ đồ xử trí cấp cứu phản vệ (Phụ lục III, Phụ lục X)',
    donVi: 'bản',
    soLuong: '01',
  },
  {
    stt: '2',
    noiDung: 'Bơm kim tiêm vô khuẩn loại 10ml',
    donVi: 'cái',
    soLuong: '02',
  },
  {
    stt: '3',
    noiDung: 'Bơm kim tiêm vô khuẩn loại 5ml',
    donVi: 'cái',
    soLuong: '02',
  },
  {
    stt: '4',
    noiDung: 'Bơm kim tiêm vô khuẩn loại 1ml',
    donVi: 'cái',
    soLuong: '02',
  },
  {
    stt: '5',
    noiDung: 'Kim tiêm 14-16g',
    donVi: 'cái',
    soLuong: '02',
  },
  {
    stt: '6',
    noiDung: 'Bông tiệt trùng tẩm cồn',
    donVi: 'gói',
    soLuong: '01',
  },
  {
    stt: '7',
    noiDung: 'Dây garo',
    donVi: 'cái',
    soLuong: '02',
  },
  {
    stt: '8',
    noiDung: 'Adrenalin 1mg/1ml',
    donVi: 'ống',
    soLuong: '05',
  },
  {
    stt: '9',
    noiDung: 'Methylprednisolon 40mg',
    donVi: 'lọ',
    soLuong: '02',
  },
  {
    stt: '10',
    noiDung: 'Diphenhydramin 10mg',
    donVi: 'ống',
    soLuong: '05',
  },
  {
    stt: '11',
    noiDung: 'Nước cất 10ml',
    donVi: 'ống',
    soLuong: '03',
  },
];
class Content extends Component {
  render() {
    return (
      <div>
        <h3><strong>1.2.1. Nguyên tắc chung</strong></h3>
        <p align="justify">- Tất cả các trường hợp phản vệ phải được phát hiện sớm, xử trí khẩn cấp, kịp thời, ngay tại chỗ và theo dõi liên tục, ít nhất trong vòng 24h.</p>
        <p align="justify">- Bác sĩ, điều dưỡng, hộ sinh viên, kỹ thuật viên, nhân viên y tế khác phải xử trí ban đầu cấp cứu phản vệ.</p>
        <p align="justify">- Adrenalin là thuốc thiết yếu, quan trọng hàng đầu cứu sống người bệnh bị phản vệ, phải được tiêm bắp ngay khi chẩn đoán phản vệ từ độ II trở lên.</p>
        <h3><strong>1.2.2. Xử trí phản vệ nhẹ (độ I): Dị ứng nhưng có thể chuyển thành nặng hoặc nguy kịch:</strong></h3>
        <p align="justify">- Sử dụng thuốc Methylprednisolon hoặc diphenhydramin uống hoặc tiêm tùy tình trạng người bệnh.</p>
        <p align="justify">- Tiếp tục theo dõi ít nhất 24 giờ để xử trí kịp thời.</p>

        <h3><strong>1.2.3.  Phác đồ xử trí cấp cứu phản vệ mức nặng và nguy kịch (độ II, III)</strong></h3>
        <p align="justify">Phản vệ độ II có thể nhanh chóng chuyển sang độ III, độ IV, Vì vậy, phải khẩn trương, xử trí đồng thời theo diễn biến bệnh:</p>
        <p align="justify">1. Ngừng ngay thuốc hoặc dị nguyên (nếu có)</p>
        <p align="justify">2. Tiêm hoặc truyền adrenalin.</p>
        <p align="justify"><i>* Phác đồ sử dụng adrenalin và truyền dịch:</i></p>
        <p align="justify">- Mục tiêu: nâng và duy trì ổn định HA tối đa của người lớn lên ≥ 90mmHg, trẻ em ≥ 70mmHg và không còn các dấu hiệu về hô hấp như thở rít, khó thở; dấu hiệu về tiêu hóa như nôn mửa, ỉa chảy.</p>
        <p align="justify">a) Thuốc adrenalin 1mg = 1ml = 1 ống, tiêm bắp:</p>
        <p align="justify">- Trẻ sơ sinh hoặc trẻ nhỏ hơn 10kg: 0,2ml (tương đương 1/5 ống)</p>
        <p align="justify">- Trẻ khoảng 10 kg: 0,25ml (tương đương 1/4 ống)</p>
        <p align="justify">- Trẻ khoảng 20 kg: 0,3ml (tương đương 1/3 ống)</p>
        <p align="justify">- Trẻ &gt; 30kg: 0,5ml (tương đương 1/2 ống)</p>
        <p align="justify">- Người lớn: 0,5-1ml ( tương đương ½-1 ống)</p>
        <p align="justify">b) Theo dõi huyết áp 3-5 phút/lần</p>
        <p align="justify">c) Tiêm nhắc lại Adrenalin như trên 3-5 phút/lần cho đến khi mạch, huyết áp ổn định.</p>
        <p align="justify">3. Cho người bệnh nằm đầu thấp, nghiêng trái nếu có nôn</p>
        <p align="justify">4. Thở ô xy: người lớn 6-101/phút, trẻ em 2-41/phút qua mặt nạ hở.</p>
        <p align="justify">5. Đánh giá tình trạng hô hấp, tuần hoàn, ý thức và các biểu hiện ở da, niêm mạc của người bệnh.</p>
        <p align="justify">- Ép tim ngoài lồng ngực và bóp bóng (nếu ngừng hô hấp, tuần hoàn)</p>
        <p align="justify">- Đặt nội khí quản hoặc mở khí quản cấp cứu (nếu khó thở thanh quản)</p>
        <p align="justify">6. Thiết lập đường truyền Adrenalin tĩnh mạch với dây truyền thông thường nhưng kim to (cỡ 14 hoặc 16G) hoặc đặt catheter tĩnh mạch và một đường truyền tĩnh mạch thứ hai để truyền dịch nhanh.</p>
        <p align="justify">7. Hội ý với các đồng nghiệp, tập trung xử lý, báo cáo cấp trên, hội chẩn với bác sĩ chuyên khoa cấp cứu, hồi sức và/hoặc chuyên khoa dị ứng (nếu có).</p>

        <h3><strong>1.2.4. Xử trí tiếp theo</strong></h3>
         <p align="justify"><i>* Hỗ trợ hô hấp, tuần hoàn: Tùy mức độ suy tuần hoàn, hô hấp có thể sử dụng một hoặc các biện pháp sau đây:</i></p>
         <p align="justify">a) Thở oxy qua mặt nạ: 6-10 lít/phút cho người lớn, 2-4 lít/phút ở trẻ em.</p>
         <p align="justify">b) Bóp bóng AMBU có oxy.</p>
         <p align="justify">c) Đặt ống nội khí quản thông khí nhân tạo có ô xy nếu thở rít tăng lên không đáp ứng với adrenalin.</p>
         <p align="justify">d) Mở khí quản nếu có phù thanh môn-hạ họng không đặt được nội khí quản.</p>
         <p align="justify">đ) Truyền tĩnh mạch chậm: aminophyllin 1mg/kg/giờ hoặc salbutamol 0,1 µg/kg/phút hoặc terbutalin 0,1 µg/kg/phút (tốt nhất là qua bơm tiêm điện hoặc máy truyền dịch).</p>
         <p align="justify">e) Có thể thay thế aminophyllin bằng salbutamol 5mg khí dung qua mặt nạ hoặc xịt họng salbutamol 100µg người lớn 2-4 nhát/lần, trẻ em 2 nhát/lần, 4-6 lần trong ngày.</p>
         <p align="justify"><i>* Nếu không nâng được huyết áp theo mục tiêu sau khi đã truyền đủ dịch và adrenalin, có thể truyền thêm dung dịch keo (huyết tương, albumin hoặc bất kỳ dung dịch cao phân tử nào sẵn có).</i></p>

        <h3><strong>1.2.5. Hộp thuốc cấp cứu phản vệ [4]</strong></h3>
        <Table columns={columns} dataSource={data} size="small" bordered="true" />

        <h3><strong>1.2.6. Trang thiết bị y tế và thuốc tối thiểu cấp cứu phản vệ tại cơ sở khám bệnh, chữa bệnh [4]</strong></h3>
        <p align="justify">1. Oxy</p>
        <p align="justify">2. Bóng AMBU và mặt nạ người lớn và trẻ nhỏ</p>
        <p align="justify">3. Bơm xịt salbutamol.</p>
        <p align="justify">4. Bộ đặt nội khí quản và/hoặc bộ mở khí quản và/hoặc mask thanh quản.</p>
        <p align="justify">5. Nhũ dịch Lipid 20% lọ 100ml (02 lọ) đặt trong tủ thuốc cấp cứu tại nơi sử dụng thuốc gây tê. gây mê.</p>
        <p align="justify">6. Các thuốc chống dị ứng đường uống.</p>
        <p align="justify">7. Dịch truyền: natriclorid 0,9%./.</p>

      </div>
    );
  }
}

export default Content;
