import { Component } from 'react';

class Content extends Component {
  render() {
    return (
      <div>
        <p align="justify">Trong lĩnh vực y tế, nhiều loại thuốc được đưa vào cơ thể bằng bất cứ đường nào đều có thể gây sốc phản vệ và dẫn đến tử vong. Ở người có cơ địa dị ứng. sốc phản vệ có thể xảy ra ngay sau khi mới dùng thuốc lần đầu, hoặc sau khi dùng thuốc vài ba lần. Một người đã làm test nội bì với kết quả âm tính vẫn có thể bị sốc phản vệ khi dùng thuốc đó trong những lần dùng tiếp theo [1].
        <br /><br />Tỷ lệ mắc sốc phản vệ ở châu  u là 4-5 trường hợp/10.000 dân, ở Mỹ là 58,9 trường hợp/100.000 dân trong những năm gần đây. Ở Việt Nam tuy chưa có thống kê song sốc phản vệ do thuốc vẫn xảy ra thường xuyên ở nhiều cơ sở chăm sóc y tế, trong đó nhiều trường hợp đã tử vong. Còn tại Bệnh viện Bạch Mai, mỗi năm khoa Hồi sức tích cực tiếp nhận khoảng 50 - 60 trường hợp sốc phản vệ được chuyển đến từ các bệnh viện khác. Con số này tăng nhiều so với 5-10 năm trước [6].
        <br /><br />Theo PGS.TS Nguyễn Gia Bình. Trưởng khoa Hồi sức tích cực (Bệnh viện Bạch Mai), Chủ tịch Hội Hồi sức cấp cứu và chống độc Việt Nam cho rằng chi phí thuốc cấp cứu sốc phản vệ không đắt nhưng quan trọng là thời gian cấp cứu kịp thời. Hiện nay, tình trạng dị ứng, sốc phản vệ xảy ra nhiều hơn, diễn biến nguy hiểm hơn do người bệnh cần sử dụng nhiều thiết bị y tế, sử dụng nhiều loại thuốc [6].
        <br /><br />Tai biến và tử vong do sốc phản vệ có thể giảm khi thầy thuốc có đầy đủ kiến thức về sốc phản vệ, khai thác kỹ tiền sử dị ứng của người bệnh: chỉ định thuốc thận trọng, đặc biệt luôn chuẩn bị sẵn sàng các phương tiện cấp cứu sốc phản vệ. Ngày 29/12/2017 Bộ Y tế đã ban hành Thông tư số 51/2017/TT-BYT về hướng dẫn phòng, chẩn đoán và xử trí phản vệ. Thông tư này có hiệu lực từ ngày 15/02/2018 và thay thế cho Thông tư số 08/1999/TT-BYT ngày 4 tháng 5 năm 1999 của Bộ trưởng Bộ Y tế về hướng dẫn phòng và cấp cứu sốc phản vệ [1] [5]. Điều dưỡng giữ một vai trò rất quan trọng trong việc phát hiện sớm và xử lý kịp thời các trường hợp sốc phản vệ. Bởi vì, họ luôn là người thường xuyên và trực tiếp thực hiện y lệnh thuốc điều trị của bác sỹ đối với người bệnh. Trong những năm gần đây, tình hình bệnh tật ngày một gia tăng và có nhiều diễn biến phức tạp, vấn đề an toàn người bệnh luôn được các bệnh viện quan tâm và đặt lên hàng đầu. Viện  Y học Phòng không-Không quân đã luôn thực hiện tốt công tác huấn luyện, giám sát, kiểm tra đối với đội ngũ điều dưỡng trong thực hiện các văn bản của Bộ Y tế về phòng và xử trí cấp cứu sốc phản vệ. Tuy nhiên. chưa có nghiên cứu nào tại Viện đánh giá thực trạng kiến thức của điều dưỡng trong phòng và xử trí sốc phản vệ theo Thông tư 51/2017/TT-BYT. Để có cơ sở đánh giá và tìm ra các giải pháp nhằm đảm bảo an toàn người bệnh, chúng tôi tiến hành nghiên cứu về <strong>“Thực trạng kiến thức dự phòng, chẩn đoán và xử trí phản vệ theo Thông tư 51/2017/TT-BYT của điều dưỡng Viện Y học Phòng không-Không quân năm 2020”</strong> với mục tiêu:
        <br /><br />1. Mô tả thực trạng về kiến thức phòng. chẩn đoán và xử trí phản vệ theo Thông tư 51/2017/TT-BYT của điều dưỡng Viện Y học Phòng không-Không quân năm 2020.
        <br /><br />2. Phân tích một số yếu tố liên quan về kiến thức phòng, chẩn đoán và xử trí phản vệ của đối tượng nghiên cứu.</p>
      </div>
    );
  }
}

export default Content;
