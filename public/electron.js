const electron = require('electron');

const { app } = electron;
const { BrowserWindow } = electron;

const path = require('path');
const isDev = require('electron-is-dev');

let mainWindow;

require('update-electron-app')({
  repo: 'kitze/react-electron-example',
  updateInterval: '1 hour',
});

function createWindow() {
  mainWindow = new BrowserWindow({ show: false, webPreferences: { nodeIntegration: true } });
  mainWindow.maximize();
  mainWindow.show();
  mainWindow.loadURL(
    isDev
      ? 'http://localhost:3000'
      : `file://${path.join(__dirname, '../build/index.html#/home')}`,
  );

  // mainWindow.webContents.openDevTools();

  // eslint-disable-next-line no-return-assign
  mainWindow.on('closed', () => (mainWindow = null));
}

app.on('ready', createWindow);

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow();
  }
});
